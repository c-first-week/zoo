﻿using System;

namespace Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal[] someAnimals = { new Animal("Bob"), 
                                    new Animal("Zoe"), 
                                    new Animal("Steve"), 
                                    new Animal("Banjo Joe") };

            foreach(var animal in someAnimals)
            {
                Console.WriteLine(animal.name);
            }

            ForestMouse theSuperHeroMouse = new ForestMouse("Lightning-Zero");
            theSuperHeroMouse.makeSound();
            ForestMouse babySuperMouse = (ForestMouse) theSuperHeroMouse.procreate("Wilhelm");

        }
            
    }
}
