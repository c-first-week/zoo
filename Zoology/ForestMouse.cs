﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class ForestMouse : Mouse
    {
        public ForestMouse(string name) : base(name) { }

        public override Animal procreate(string name)
        {
            return new ForestMouse(name);
        }
    }
}
