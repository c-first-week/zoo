﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class Animal
    {
        float heightMeters = 2.0f;
        public float height { get => heightMeters; set => heightMeters = value; }
        
        float widthMeters = 1.0f;
        public float width { get => widthMeters; set => widthMeters = value; }
        
        float speedKMpS = 4.1f;
        public float speed { get => speedKMpS; set => speedKMpS = value; }
        
        private string animalName = "Flipper";
        public string name { get => animalName; set => animalName = value; }

        public Animal(string aName)
        {
            this.animalName = aName;
        }

        public Animal(float height, float width)
        {
            this.height = height;
            this.width = width;
        }

        public Animal(float height, float width, float kmps)
        {
            this.height = height;
            this.width = width;
            this.speed = kmps;
        }


        public void makeSound() {
            Console.WriteLine("Moo, bah, ahonkadonk");
        }
    }
}
