﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    abstract class Mouse : Animal, IBaseAnimalMethods
    {
        public Mouse(float height, float width) : base(height, width) { }
        public Mouse(string name) : base(name) { }
        public Mouse(float height, float width, float kmps) : base(height, width, kmps) { }


        public abstract Animal procreate(string name);
    }
}
