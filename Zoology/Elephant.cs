﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class Elephant : Animal, IBaseAnimalMethods
    {
        Elephant(float height, float width) : base(height, width) { }
        Elephant(string name) : base(name) { }
        Elephant(float height, float width, float kmps) : base(height, width, kmps) { }

        public Animal procreate(string name)
        {
            return new Elephant(name);
        }
    }
}
